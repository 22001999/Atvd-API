//
//  ViewController.swift
//  MinhasMusicas
//
//  Created by COTEMIG on 25/08/22.
//

import UIKit
import Alamofire
import Kingfisher

struct Character : Decodable {
    let name: String
    let actor: String
    let image: String
}

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var CharacterList:[Character] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.CharacterList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MyCell
        let musica = self.CharacterList[indexPath.row]
        
        cell.name.text = musica.name
        cell.nameActor.text = musica.actor
        cell.imageCharacter.image = UIImage(named: musica.image)
        
        return cell
    }
 
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        self.tableView.delegate = self
        
        self.CharacterList.append(Character(name: "Pontos Cardeais", actor: "Álbum Vivo!", image: "capa_alceu_pequeno"))
        self.CharacterList.append(Character(name: "Menor Abandonado", actor: "Álbum Patota de Cosme",  image: "capa_zeca_pequeno"))
        self.CharacterList.append(Character(name: "Tiro ao Álvaro", actor: "Álbum Adoniran Barbosa e Convidados", image: "capa_adoniran_pequeno"))
    }
    
    func getNovoCharacter() {
            AF.request("https://hp-api.onrender.com/api/characters").responseDecodable(of: [Character].self) { response in
                if let character = response.value {
                    self.CharacterList = character
                }
            }
        }


}

